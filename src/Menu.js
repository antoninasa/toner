import React, { Component } from 'react';
import { Link } from 'react-router';

class Menu extends Component {
  render() {
    return (
      <div>
        <p><Link to="/">Выход</Link></p>
        <p><Link to="/about">About</Link></p>
      </div>
    );
  }
}

export default Menu;
