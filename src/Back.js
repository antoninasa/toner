import React, { Component } from 'react';
import { Link } from 'react-router';

class Back extends Component {
  render() {
    return (
      <div>
        <Link to="/">Выход</Link>
        <Link to="/about">About</Link>
      </div>
    );
  }
}

export default Back;
