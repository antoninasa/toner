import React, { Component } from 'react';
import './App.css';

import RegistrationForm from './RegistrationForm'

class App extends Component {
  render() {
    return (
      <div className="container">
      Приветствуем Вас!
      Пожалуйста, введите свой e-mail, чтобы продолжить:
      <RegistrationForm />
      </div>
    );
  }
}

export default App;
